package ec.sevolutivo.semejora.kafka.producer;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProducerController {

    private final ProducerService producerService;

    public ProducerController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @GetMapping("/greet/{name}")
    public String greet(@PathVariable(name = "name") String name) {
        return "Hello " + name;
    }

    @GetMapping("/local-digest/{value}")
    public void localDigest(@PathVariable(value = "value") String value) {
        producerService.localDigest(value);
    }

    @GetMapping("/remote-digest/{value}")
    public void remoteDigest(@PathVariable(value = "value") String value) {
        producerService.remoteDigest(value);
    }


    @GetMapping("/send/{value}")
    public void send(@PathVariable(value = "value") String value) {
        producerService.sendMessage(value);
    }

    @GetMapping("/process/{value}")
    public void process(@PathVariable(value = "value") Long value) {
        producerService.process(value);
    }

}
