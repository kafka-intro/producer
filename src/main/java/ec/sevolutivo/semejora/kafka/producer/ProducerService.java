package ec.sevolutivo.semejora.kafka.producer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service
@Transactional
public class ProducerService {

    private final Logger LOG = LoggerFactory.getLogger(ProducerService.class);
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    ProducerService(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Value("${application.topic}")
    private String topic;


    void sendMessage(String message) {
        LOG.info("Sending : {}", message);
        LOG.info("--------------------------------");

        kafkaTemplate.send(this.topic, message);
    }


    public void process(Long value) {

        double result = Math.log10((double)value);

        LOG.info("Result: {}", result);

    }


    public void localDigest(String value) {

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            digest.reset();
            digest.update(value.getBytes("utf8"));
            String toReturn = String.format("%0128x", new BigInteger(1, digest.digest()));

            LOG.info("SHA512: {}", toReturn);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void remoteDigest(String value) {
        kafkaTemplate.send(this.topic, value);
    }
}
